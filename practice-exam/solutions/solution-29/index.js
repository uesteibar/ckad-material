const http = require('http');
const fs = require('fs');
const app = new http.Server();

app.on('request', (req, res) => {
    // Write request log
    fs.appendFileSync('/var/log/requests.txt', `${new Date}`);

    res.writeHead(200, { 'Content-Type': 'text/plain' });
    res.write('Hello World');
    res.end('\n');
});

app.listen(8000, () => {
  console.log(`Server listening on port 8000`);
});