$ kubectl get pods --namespace=ggckad-s1 --selector='tier=frontend'
NAME                                READY     STATUS    RESTARTS   AGE
deployment-one-7dccdf4f89-726xj     1/1       Running   0          20s
deployment-one-7dccdf4f89-ltgv9     1/1       Running   0          20s
deployment-one-7dccdf4f89-mzwld     1/1       Running   0          20s
deployment-three-7886bcf64d-bnp5g   1/1       Running   0          20s
deployment-three-7886bcf64d-t6lxb   1/1       Running   0          20s

$ kubectl get pods --namespace=ggckad-s1 --selector='app=nginx'
NAME                              READY     STATUS    RESTARTS   AGE
deployment-one-7dccdf4f89-726xj   1/1       Running   0          34s
deployment-one-7dccdf4f89-ltgv9   1/1       Running   0          34s
deployment-one-7dccdf4f89-mzwld   1/1       Running   0          34s

No, all pods with tier=frontend also have app=nginx
