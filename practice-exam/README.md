
> Please avoid sharing this practice exam.
> It's a key part of the course material for my book,
> Golden Guide to Kubernetes Application Development.
>
> If you know someone who really wants it but can't 
> afford to buy the package, please send me an email. 
> We can make an arrangement.



# Introduction

This is a practice exam designed to help you prepare for the CKAD exam. It's a collection of sample questions around the topics in the CKAD exam curriculum.

# Setup and configuration

## Pre-requisites

This practice exam is written assuming you are using minikube on your local machine, but it should be useful regardless of your Kubernetes
cluster.

It uses the following Kubernetes namespaces -- please ensure there are no conflicts with your environment.

* ggckad-s0
* ggckad-s1
* ggckad-s2
* ggckad-s2-rq
* ggckad-s3
* ggckad-s4
* ggckad-s5
* ggckad-s6
* ggckad-s7
* ggckad-s8

## Setup

Run the setup script in this directory.

```
./setup-exam.sh
```

This script will perform configuration on your Kubernetes cluster, like setting up namespaces, creating resources, and downloading images so they're pre-cached.

After successful execution of the script, you should see the following output.

```
# ... truncated output
* * *

Kubernetes cluster prepared for the practice exam.
Good luck!
```

# Exam instructions

* The questions for the exam are in the file `questions.txt`.
* Some questions will instruct you to use certain resources,
  available in the `resources` directory.
* Solutions are available in the `solutions` directory.
* Pay particular attention to which namespace a question needs
  you to operate in. This may be something you don't normally
  do, but is definitely part of the CKAD exam, hence this
  practice exam tests your ability to work in a namespace.
* There is an answer sheet (`answer-sheet.txt`) where you are 
  welcome to write answers and take notes. However, you might
  prefer to answer each question in a new file. It's up to you.
* The solutions are not the only way to complete the question, 
  just the way that I would have done it. I'd love for you to 
  post more solutions in the Github repo!
  In particular, I'm personally more comfortable using YAML than
  writing kubectl commands directly. In the real exam you can
  generally use either -- so both solutions are correct as long
  as the result is the same.

Thirty questions. Three hours. Good luck.

## Exam Breakdown

The exam is made up of the following components.

* Section 1 - Core Concepts (13%) - Question 1-4
* Section 2 - Configuration (18%) - Question 5-9
* Section 3 - Multi-Container Pods (10%) - Question 10-12
* Section 4 - Observability (18%) - Question 13-17
* Section 5 - Pod Design (20%) - Question 18-23
* Section 6 - Services & Networking (13%) - Question 24-27
* Section 7 - State Persistence (8%) - Question 28
* Section 8 - Challenge Questions
