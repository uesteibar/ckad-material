// Wait sixty seconds, then start answering requests to /health
var http = require('http');

var finishedStarting = false;

console.log('Application starting...');

setTimeout(function() {
    console.log('Application ready.');
    finishedStarting = true;
}, 60 * 1000);

var server = http.createServer(function(request, response) {
    if (finishedStarting && request.url === '/health') {
        response.statusCode = 200;
        response.setHeader('Content-Type', 'text/plain');
        response.end('Health - OK');
        return;
    }

    response.statusCode = 400;
    response.setHeader('Content-Type', 'text/plain');
    response.end('Health - Not OK');
    response.end('');
});

server.listen(8080, function() {
  console.log('  Using port 8080');
});

