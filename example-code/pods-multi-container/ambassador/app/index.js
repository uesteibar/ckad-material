const express = require('express');
const app = express();
const request = require('request');

// Our ambassador container runs on port 8080.
const AMBASSADOR_URL = 'http://localhost:8080/'

app.get('/', (req, res) => {
  // We need to connect to an external resource, however we want to be able to
  // dynamically switch our external connection without changing the application
  // container.

  // The application connects to the external resource via the ambassador, which
  // is essentially a proxy.

  // User request => application => ambassador => external service

  return request(AMBASSADOR_URL).pipe(res);
});

app.listen(3000, () => console.log('Application listening on port 3000.'));
