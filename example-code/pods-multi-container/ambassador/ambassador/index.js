const express = require('express');
const app = express();
const request = require('request');

app.get('/', (req, res) => {
  // Just to illustrate dynamic switching of external services, pick a 
  // random fake JSON REST API service.
  let url;

  if (Math.random() > 0.5) {
    url = 'https://reqres.in/api/users?page=2';
  } else {
    url = 'https://jsonplaceholder.typicode.com/posts/1';
  }

  return request(url).pipe(res);
});

app.listen(8080, () => console.log('Ambassador listening on port 8080.'));
