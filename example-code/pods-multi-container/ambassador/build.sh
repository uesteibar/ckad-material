#!/bin/sh

# Building these Docker images to run on your local minikube cluster?
# Remember to execute `eval $(minikube docker-env)` to use the Docker
# daemon from minikube for building.

# Build the app and ambassador Docker images
cd app
docker build . -t "ambassador-example-app:1.0.0"

cd ..

cd ambassador

docker build . -t "ambassador-example-ambassador:1.0.0"

cd ..
