![hero](./design-drafts/hero-3.png)

<br />

# Golden Guide to Kubernetes Application Development

## All the bonus content

You’ve unlocked access to a wide array of extra content contained in each directory in this repo.

📖 `book` — the **complete book content** in both PDF and source formats, plus revision history of each PDF export

⏳ `practice-exam` — a thirty question, three hour **practice exam** with complete solutions to get you ready for your real CKAD exam

🖥️ `example-code` — **full code examples** used in the book, plus extra utilities

🎨 `diagrams` — **high resolution source diagrams** from the book

📝 `design-drafts` — behind the scenes **design resources**, covers, and experiments

<br />

## Tooling

For the exam you'll be asked to use only two browser tabs, one with [official Kubernetes documentation](https://kubernetes.io/docs/home/) and one with a browser based shell called [GateOne](http://liftoffsoftware.com/Products/GateOne).

# Installing GateOne

* Enable remote login in you computer for your current user. Instructions [here](https://www.booleanworld.com/access-mac-ssh-remote-login/)
* Download the latest version in .tar.gz format from [here](https://github.com/liftoff/GateOne/releases)
* Run the following command in the folder you downloaded the tar file
```
tar zxvf gateone*.tar.gz; cd gateone*; sudo python setup.py install
```
* Install pip (may not be needed)
```
sudo python -m ensurepip
```
* Install tornado v 2.4.1 using pip (you may also need to install pip)
```
pip install tornado===2.4.1
```
* run the following command
```
sudo python /opt/gateone/gateone.py --disable_ssl --port=80
```




## Interesting links:

https://github.com/dgkanatsios/CKAD-exercises

https://medium.com/@KevinHoffman/taking-the-certified-kubernetes-administrator-exam-eeab17d65476
